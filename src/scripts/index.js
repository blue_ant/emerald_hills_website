let homeBanner = new Swiper(".home-banner-list", {
    effect: "slide",
    slidesPerView: 1,
    autoplay: {
        delay: 4200,
        disableOnInteraction: false,
    },
    speed: 1000,
    //mousewheel: true,
    pagination: {
        el: ".home-banner-list .swiper-pagination",
        type: "bullets",
        clickable: true,
    },
    resistance: true,
    resistanceRatio: 0,
});

$(".video-about-btn").fancybox({
    smallBtn: true,
    modal: false,
    hideScrollbar: false,
    backFocus: false,
    buttons: [],
    touch: false,
    baseClass: "video-about-modal",
    btnTpl: {
        smallBtn:
            '<button data-fancybox-close class="close-gallery close-gallery_video" title="{{CLOSE}}">' +
            '<img src="img/close-popup-dark.svg" alt=""></img>' +
            "</button>",
    },
    afterClose: function(instance, slide) {
        let scroll = $(document).scrollTop();
        $(window).scrollTo(scroll);
    },
});

let hideScrollForBannerSlide = () => {
    let activeSlide = $('.home-banner-list').find('.swiper-slide-active');
    let scrollingIcon = $('.home-banner__scrolling');
    let pagination = $('.home-banner .swiper-pagination');
    let slideIsBanner = activeSlide.hasClass('banner-slide');

    if (slideIsBanner) {
        scrollingIcon.addClass('home-banner__scrolling_hidden');
        pagination.addClass('swiper-pagination_hidden');
    } else {
        scrollingIcon.removeClass('home-banner__scrolling_hidden');
        pagination.removeClass('swiper-pagination_hidden');
    }
}

hideScrollForBannerSlide();

homeBanner.on('slideChangeTransitionEnd', function () {
    console.log('slide change')
    hideScrollForBannerSlide();
});
