$(function () {
    if ($('.showroom').length) {
        $('.showroom__overlay').on('click', function () {
            let fullFrame = $('.showroom-fullFrame');
            let srcFrame = fullFrame.data('frame');
            if (fullFrame.find('iframe').length === 0) {
                fullFrame.append(`<iframe class="showroom-fullFrame__iframe" src="${srcFrame}"></iframe>`)
            }
            fullFrame.fadeIn(400);
        });

        $('.showroom-fullFrame__close').on('click', function () {
            $('.showroom-fullFrame').fadeOut(400);
        });

        $('[data-scrollTo="showroom"]').on('click', function (event) {
            event.preventDefault();
            let top = $('[data-scroll="showroom"]').offset().top;
            $('body,html').animate({scrollTop: top - 90}, 1800);

        });
    }
});