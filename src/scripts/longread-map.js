


const longreadStyleMap = [{
    "featureType": "administrative.land_parcel",
    "elementType": "all",
    "stylers": [{
        "visibility": "off"
    }]
},
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
            "color": "#f2f2f2"
        }]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "color": "#fcb943"
        }]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#ffedcc"
        }]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
            "hue": "#ffcb00"
        },
            {
                "weight": "5.00"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        },
            {
                "color": "#bfc9c8"
            },
            {
                "weight": "3.00"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#cccfcf"
        }]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
            "color": "#7dcac5"
        }]
    }
];




const RouteAutoMarkers = [
    {
        lat: 55.834565,
        lng: 37.282611,
        icon: {
            url: '/img/longread-5/route-icon-finish.png',
            anchor: new google.maps.Point(12, 10)
        },
        type: 'object-finish'
    },
    {
        lat: 55.835906,
        lng: 37.282262,
        icon: {
            url: '/img/contacts/marker-object.png'
        },
        type: 'object'
    },
    {
        lat: 55.818986,
        lng: 37.330457,
        icon: {
            url: '/img/longread-5/route-auto-1.png',
            anchor: new google.maps.Point(10, 73)
        },
        type: 'route-auto-1'
    },
    {
        lat: 55.832568,
        lng: 37.395017,
        icon: {
            url: '/img/longread-5/route-icon-finish.png',
            anchor: new google.maps.Point(5, 8)
        },
        type: 'route-auto-1'
    },

    {
        lat: 55.799583,
        lng: 37.339032,
        icon: {
            url: '/img/longread-5/route-auto-2.png',
            anchor: new google.maps.Point(20, 70)
        },
        type: 'route-auto-2'
    },
    {
        lat: 55.789981,
        lng: 37.370710,
        icon: {
            url: '/img/longread-5/route-icon-finish.png',
            anchor: new google.maps.Point(5, 10)
        },
        type: 'route-auto-2'
    },
    {
        lat: 55.846461,
        lng: 37.360510,
        icon: {
            url: '/img/longread-5/mitino-metro.png',
            anchor: new google.maps.Point(10, 35)
        },
        type: 'route-transport-1'

    },
    {
        lat: 55.818986,
        lng: 37.330457,
        icon: {
            url: '/img/longread-5/mitino-bus.png',
            anchor: new google.maps.Point(10, 58)
        },
        type: 'route-transport-1'
    },
    {
        lat: 55.826645,
        lng: 37.437334,
        icon: {
            url: '/img/longread-5/tushino-metro.png',
            anchor: new google.maps.Point(10, 30)
        },
        type: 'route-transport-2'

    },
    {
        lat: 55.818986,
        lng: 37.330457,
        icon: {
            url: '/img/longread-5/tushino-bus.png',
            anchor: new google.maps.Point(10, 58)
        },
        type: 'route-transport-2'
    },
    {
        lat: 55.850592,
        lng: 37.439573,
        icon: {
            url: '/img/longread-5/shodnenskaya-metro.png',
            anchor: new google.maps.Point(10, 30)
        },
        type: 'route-transport-3'

    },
    {
        lat: 55.818986,
        lng: 37.330457,
        icon: {
            url: '/img/longread-5/shodnenskaya-bus.png',
            anchor: new google.maps.Point(10, 58)
        },
        type: 'route-transport-3'
    },
    {
        lat: 55.834565,
        lng: 37.282611,
        icon: {
            url: '/img/longread-5/hospital.png',
            anchor: new google.maps.Point(16, 0)
        },
        type: 'route-transport'
    },
    {
        lat: 55.823364,
        lng: 37.246829,
        icon: {
            url: '/img/longread-5/opaliha.png',
            anchor: new google.maps.Point(16, 0)
        },
        type: 'route-easy-metro'
    },
    {
        lat: 55.826460,
        lng: 37.271805,
        icon: {
            url: '/img/longread-5/easy-metro.png',
            anchor: new google.maps.Point(30, 45)
        },
        type: 'route-easy-metro'
    },
    {
        lat: 55.823364,
        lng: 37.246829,
        icon: {
            url: '/img/longread-5/opaliha.png',
            anchor: new google.maps.Point(12, 4)
        },
        type: 'route-railway'
    },
    {
        lat: 55.814573,
        lng: 37.303276,
        icon: {
            url: '/img/longread-5/krasnogorskaya.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },
    {
        lat: 55.815612,
        lng: 37.341847,
        icon: {
            url: '/img/longread-5/pavshino.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },
    {
        lat: 55.833381,
        lng: 37.397974,
        icon: {
            url: '/img/longread-5/trikotazhnaya.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },
    {
        lat: 55.826876,
        lng: 37.441160,
        icon: {
            url: '/img/longread-5/tushino.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },
    {
        lat: 55.814183,
        lng: 37.476667,
        icon: {
            url: '/img/longread-5/streshnevo.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },
    {
        lat: 55.815125,
        lng: 37.498098,
        icon: {
            url: '/img/longread-5/leningradskaya.png',
            anchor: new google.maps.Point(12, 6)
        },
        type: 'route-railway'
    },

];

let startRoute = [55.834565, 37.282611]






let longreadMap;


longreadMap = new GMaps({
    div: ".longread-routes__map",
    lat: 55.835906,
    lng: 37.282262,
    zoom: 12,
    styles: longreadStyleMap,
});


$('.longread-routes-btns__item').click(function (e) {
    e.preventDefault();
    $('.longread-routes-btns__item').removeClass('longread-routes-btns__item_active');
    $(this).toggleClass('longread-routes-btns__item_active');
});

RouteAutoMarkers.forEach(function (item, i){
    longreadMap.addMarker({
        lat: item.lat,
        lng: item.lng,
        icon: item.icon,
        anchor: new google.maps.Point(17, 34),
        details: {
            type: item.type,
        }
    });
});

let insertedMarkers = longreadMap.markers;

let DrawAutoRouteOne = ()=> {
    longreadMap.removePolylines();
    longreadMap.drawRoute({
        origin: startRoute,
        destination: [55.832568, 37.395017],
        travelMode: 'driving',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7
    });

    for (let i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-auto-1'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
};


let DrawAutoRouteTwo = ()=> {
    longreadMap.removePolylines();
    longreadMap.drawRoute({
        origin: startRoute,
        destination: [55.789981, 37.370710],
        travelMode: 'driving',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7
    });

    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-auto-2'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
};

DrawAutoRouteOne();

$('#route-auto-1').click(function (e) {
    e.preventDefault();
    DrawAutoRouteOne();
});

$('#route-auto-2').click(function (e) {
    e.preventDefault();
    DrawAutoRouteTwo();
});



let DrawTransportRouteOne = ()=> {
    longreadMap.removePolylines();
    //митино
    longreadMap.drawRoute({
        origin: startRoute,
        destination: [55.846461, 37.360510],
        travelMode: 'driving',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7
    });

    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-transport-1'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'route-transport'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
};

let DrawTransportRouteTwo = ()=> {
    longreadMap.removePolylines();
    // тушино
    longreadMap.drawRoute({
        origin: startRoute,
        destination: [55.826645, 37.437334],
        travelMode: 'driving',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7
    });


    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-transport-2'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'route-transport'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
};


let DrawTransportRouteThree = ()=> {
    longreadMap.removePolylines();
    // сходненская
    longreadMap.drawRoute({
        origin: startRoute,
        destination: [55.850592, 37.439573],
        travelMode: 'driving',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7
    });


    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-transport-3'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'route-transport'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
};

$('#route-metro-1').click(function (e) {
    e.preventDefault();
    DrawTransportRouteOne();
});

$('#route-metro-2').click(function (e) {
    e.preventDefault();
    DrawTransportRouteTwo();
});

$('#route-metro-3').click(function (e) {
    e.preventDefault();
    DrawTransportRouteThree();
});



let DrawMetroRoute = ()=> {
    longreadMap.removePolylines();
    longreadMap.drawRoute({
        origin: [55.834386, 37.282837],
        destination: [55.823364, 37.246829],
        travelMode: 'transit',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 7,
    });
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-easy-metro'
            || insertedMarkers[i].details.type === 'object'
            || insertedMarkers[i].details.type === 'route-transport'
            || insertedMarkers[i].details.type === 'object-finish') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    };
    longreadMap.fitZoom();
}



let DrawRailwayRoute = ()=> {
    longreadMap.removePolylines();

    let DashedLine = {
        path: 'M 0,-1 0,1',
        strokeOpacity: 1,
        scale: 4
    };

    longreadMap.drawRoute({
        origin: [55.823329, 37.246835],
        destination: [55.815143, 37.498182],
        travelMode: 'transit',
        strokeColor: '#fd7b23',
        strokeOpacity: 1,
        strokeWeight: 0,
        icons: [{
            icon: DashedLine,
            offset: '0',
            repeat: '20px'
        }],
    });
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 'route-railway' || insertedMarkers[i].details.type === 'object') {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    longreadMap.fitZoom();
    longreadMap.setZoom(12)
};








$('.longread-routes-links__item').click(function (e) {
    e.preventDefault();
    $('.longread-routes-links__item').removeClass('longread-routes-links__item_active');
    $(this).addClass('longread-routes-links__item_active');
    let linkText = $(this).data('route-text');
    $('.longread-routes__descr').removeClass('longread-routes__descr_active');
    $('#' + linkText).addClass('longread-routes__descr_active');
    $('.longread-routes-links__mobileText').removeClass('longread-routes-links__mobileText_active');
    $('[data-route-link="'+ linkText + '"]').addClass('longread-routes-links__mobileText_active');

    $('.longread-routes-btns__item').removeClass('longread-routes-btns__item_active')
    $('#' + linkText).find('.longread-routes-btns__item').eq(0).addClass('longread-routes-btns__item_active')

    switch (linkText) {
        case 'route-auto':
            DrawAutoRouteOne();
            break;
        case 'route-transport':
            DrawTransportRouteOne();
            break;
        case 'route-railway':
            DrawRailwayRoute();
            break;
        case 'route-metro':
            DrawMetroRoute();
            break;
        default:
            console.log('fail')

    }
});