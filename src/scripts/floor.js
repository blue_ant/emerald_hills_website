$('.floor__plan-item').each(function (index, el) {
  let $el = $(el);
  let data = window.floorTolltips[index];

  $el.tooltip({
    items: "[id]",
    track: true,
    tooltipClass: 'floor__plan-tooltip',
    content:
    '<div class="floor__plan-tooltip-row">' +
      '<div class="floor__plan-tooltip-number">Квартира № ' + data.num + '</div>' +
      '<div class="floor__plan-tooltip-text">' + data.rooms + ' комн.</div>' +
    '</div>' +
    '<div class="floor__plan-tooltip-row">' +
      '<div class="floor__plan-tooltip-price">' + data.price + ' \u20bd</div>' +
      '<div class="floor__plan-tooltip-text">' + data.area + ' м<span>2</span></div>' +
    '</div>'
  });

  $el.on('click', function () {
    window.location.href = $(this).attr('data-url');
  });
});