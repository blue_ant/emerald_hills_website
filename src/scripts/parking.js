$(function() {
    $('[data-select]').selectmenu({
        width: '100%',
        create: function(e, ui) {
            $('.ui-selectmenu-menu').addClass('_custom');
        }
    });

    var parkingSliderThumbs = new Swiper('.parking-slider-thumbs', {
        spaceBetween: 0,
        slidesPerView: 'auto',
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });

    var parkingSlider = new Swiper('.parking-slider', {
        spaceBetween: 50,
        thumbs: {
            swiper: parkingSliderThumbs
        }
    });

    $('#parkingChoiceBld').selectmenu({
        change: function(event, ui) {
            $(ui.item.element).trigger("change");
        },
    });
});

$('#parkingFilter').on('change', 'input, select', function() {
    $('.parking-result').addClass('parking-result_loading');
});

let parkingSorter = new Sorter(".parking-sorter");

const PARKING_PER_PAGE = 14;

let parkingTplStr = `
/*=require ./includes/chunks/parking.tpl */
`;

let parkingListBuilder = new FlatsList({
    tpl: parkingTplStr,
    $mountEl: $("#parkingResult"),
});

let $pager = $(".parking-pagination").pagination({
    itemsOnPage: PARKING_PER_PAGE,
    prevText: "&nbsp;",
    nextText: "&nbsp;",
    displayedPages: 3,
    ellipsePageSet: false,
    edges: 0,
    onPageClick: (num) => {
        filter.offset = (num - 1) * PARKING_PER_PAGE;
        console.log("pager click!!!");
        filter.$filterForm.trigger("submit");
        $("html,body").animate({ scrollTop: parkingSorter.$el.parent().offset().top }, 700, "easeInOutExpo");
        return false;
    },
});

let parkingPreloader = $('.parking-preloader');

let filter = new ParkingFilter("#parkingFilter", {
    submitHandler: ($filterForm) => {

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: PARKING_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        if (percent === 100) {
                            parkingPreloader.addClass('parking-preloader_disabled');
                        }
                    } else {
                        parkingPreloader.addClass('parking-preloader_disabled');
                    }
                    $('.parking-result').removeClass('parking-result_loading');
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                parkingListBuilder.render({
                    data: jsonResponse,
                });


                $pager.pagination("updateItems", jsonResponse.total);

                {
                    let currentPage = filter.offset / PARKING_PER_PAGE + 1;
                    $pager.pagination("drawPage", currentPage);
                }
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});
