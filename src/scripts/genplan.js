var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
        callback(evt.type == 'error', img);
        img.onload = img.onerror = img = null;
    };
    img.src = src;
};

var $start   = $('.genplan__start');
var $labels  = $('.genplan__labels');
var $numbers = $('.genplan__number');

$(".genplan__object, .genplan__number").hover(function(e) {
    var pos = $(".genplan__content").offset();
    var elem_left = pos.left;
    var elem_top = pos.top;
    var Xinner = e.clientX - elem_left;
    var Yinner = e.clientY - elem_top;

    let $this = $(this);
    let objId = $this.data('id');

    $numbers
        .css('z-index', 1)
        .filter(`[data-id=${this.dataset.id}]`)
        .css('z-index', 1030);

    if (objId === 'genplan-object_46' || objId === 'genplan-object_47' || objId === 'genplan-object_48') {
        $start.hide();
    }

    let widthLabel = $(`.genplan__label[data-id=${this.dataset.id}]`).width();

    let left = $(`.genplan__number[data-id=${this.dataset.id}]`).position().left;
    let top = $(`.genplan__number[data-id=${this.dataset.id}]`).position().top;

    $(`.genplan__render[data-id=${this.dataset.id}]`).addClass('genplan__render--active');
    $(`.genplan__number[data-id=${this.dataset.id}]`).addClass('genplan__number--active');
    $(`.genplan__label[data-id=${this.dataset.id}]`)
        .css({ top: top - 110 , left: left - widthLabel/2 })
        .addClass('genplan__label--active');
    let checkPositionLabel = $(`.genplan__label[data-id=${this.dataset.id}]`).offset().left;
    if (checkPositionLabel < 0) {
        $(`.genplan__label[data-id=${this.dataset.id}]`)
            .css({ top: top - 110 , left: left })
            .addClass('genplan__label--active2');
    }
}, function() {
    $start.show();
    $numbers.css('z-index', 1030);
    $(`.genplan__render[data-id=${this.dataset.id}]`).removeClass('genplan__render--active');
    $(`.genplan__number[data-id=${this.dataset.id}]`).removeClass('genplan__number--active');
    $(`.genplan__label[data-id=${this.dataset.id}]`).removeClass('genplan__label--active');
    $(`.genplan__label[data-id=${this.dataset.id}]`).removeClass('genplan__label--active2');
});

$(".genplan__object, .genplan__number").click(function() {
    let $bld  = $(this);
    let bldID = $bld.data('id').split("genplan-object_")[1];

    if ($bld.hasClass('_disabled')) {
        return;
    }

    window.location = $bld.data("bld-url") + "?bld[]=" + bldID;
});

loadImage($(".genplan__img-common").attr('src'), function(err, img) {
    if (err) {
        alert('image not load');
    } else {
        var ratio = img.width / img.height;
        var windowWidth = $(window).width();


        updateGenplan(ratio, windowWidth);

        $(window).resize(() => {
            windowWidth = $(window).width();
            updateGenplan(ratio, windowWidth);
        });
    }
});


function updateGenplan(ratio, windowWidth) {
    resizeImg(ratio);
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");
    if (windowWidth >= 1280) {
        genplanPosition();
        $genplan.css({ 'overflow-x': '' });
    } else {
        $genplanContent.css({ top: '', left: '' });
        $genplan.css({ 'overflow-x': 'auto' });

        $genplan.scrollLeft(($genplanContent.width() - $(window).width()) / 2);
    }
    $genplan.css('visibility', 'visible');
}

function resizeImg(ratio) {
    var genWidth = $(".genplan").outerWidth();
    var genHeight = $(".genplan").outerHeight();
    var getRatio = genWidth / genHeight;

    if (getRatio < ratio) {
        $(".genplan__content").css({ width: `${genHeight*ratio}px`, height: '100%' });
        $(".genplan__img-common").css({ width: `${genHeight*ratio}px`, height: '100%' });
    } else {
        $(".genplan__content").css({ width: '', height: '' });
        $(".genplan__img-common").css({ width: '', height: '' });
    }
}

function genplanPosition() {
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");

    var genWidth = $genplan.outerWidth();
    var genHeight = $genplan.outerHeight();

    var genContentWidth = $genplanContent.outerWidth();
    var genContentHeight = $genplanContent.outerHeight();

    $genplanContent.css({
        top: `${-(genContentHeight - genHeight)/2}px`,
        left: `${-(genContentWidth - genWidth)/2}px`,
    });
}

$('.genplan__object, .genplan__number').hover(
    function () {
        let renderId = $(this).data('id');
        $('.genplan__numbers').find("[data-id='" + renderId + "']").hide();
        $('.genplan__number-disable').show();
    },
    function () {
        $('.genplan__number').show();
        $('.genplan__number-disable').show();
    }
);


$(document).ready( function(){
    function setPosNum() {
        let windowWidth = $(window).width();
        let posNum = $('div[data-pos="gpStartEnter"]').position();
        $('.genplan__start._enter').css({top: posNum.top - 150, left: posNum.left - 147, opacity: 1});
        if (windowWidth <= 1023) {
            $('.genplan__start._enter').css({top: posNum.top - 130, left: posNum.left - 150, opacity: 1});
        }
        if (windowWidth <= 767) {
            $('.genplan__start._enter').css({top: posNum.top - 110, left: posNum.left - 93, opacity: 1});
        }
    }
    $(window).on('resize', function () {
	      setPosNum();
    }).trigger('resize');
});