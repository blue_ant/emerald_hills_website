$(".contact-offices-types").tabs({
    classes: {
        "ui-tabs": "contact-offices-types-tabs",
        "ui-tabs-nav": "contact-offices-types-tabs__nav",
        "ui-tabs-tab": "contact-offices-types-tabs__tab",
        "ui-tabs-panel": "contact-offices-types-tabs__panel",
    },
});

var winWidth = $(window).width();

if (winWidth < 768) {
    $(".contacts-header-links").appendTo(".contact-offices");
} else {
    $(".contacts-header-links").appendTo(".contacts-header-inner");
}

$(window).resize(function() {
    var winWidth = $(window).width();

    if (winWidth < 768) {
        $(".contacts-header-links").appendTo(".contact-offices");
    } else {
        $(".contacts-header-links").appendTo(".contacts-header-inner");
    }
});

var mapStyle = [
    {
        elementType: "geometry",
        stylers: [
            {
                color: "#ebe3cd",
            },
        ],
    },
    {
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#523735",
            },
        ],
    },
    {
        elementType: "labels.text.stroke",
        stylers: [
            {
                color: "#f5f1e6",
            },
        ],
    },
    {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#c9b2a6",
            },
        ],
    },
    {
        featureType: "administrative.land_parcel",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#dcd2be",
            },
        ],
    },
    {
        featureType: "administrative.land_parcel",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#ae9e90",
            },
        ],
    },
    {
        featureType: "landscape.natural",
        elementType: "geometry",
        stylers: [
            {
                color: "#dfd2ae",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "geometry",
        stylers: [
            {
                color: "#dfd2ae",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#93817c",
            },
        ],
    },
    {
        featureType: "poi.park",
        elementType: "geometry.fill",
        stylers: [
            {
                color: "#a5b076",
            },
        ],
    },
    {
        featureType: "poi.park",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#447530",
            },
        ],
    },
    {
        featureType: "road",
        elementType: "geometry",
        stylers: [
            {
                color: "#f5f1e6",
            },
        ],
    },
    {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [
            {
                color: "#fdfcf8",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [
            {
                color: "#f8c967",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#e9bc62",
            },
        ],
    },
    {
        featureType: "road.highway.controlled_access",
        elementType: "geometry",
        stylers: [
            {
                color: "#e98d58",
            },
        ],
    },
    {
        featureType: "road.highway.controlled_access",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#db8555",
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#806b63",
            },
        ],
    },
    {
        featureType: "transit.line",
        elementType: "geometry",
        stylers: [
            {
                color: "#dfd2ae",
            },
        ],
    },
    {
        featureType: "transit.line",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#8f7d77",
            },
        ],
    },
    {
        featureType: "transit.line",
        elementType: "labels.text.stroke",
        stylers: [
            {
                color: "#ebe3cd",
            },
        ],
    },
    {
        featureType: "transit.station",
        elementType: "geometry",
        stylers: [
            {
                color: "#dfd2ae",
            },
        ],
    },
    {
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [
            {
                color: "#b9d3c2",
            },
        ],
    },
    {
        featureType: "water",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#92998d",
            },
        ],
    },
];

var mapContacts;

mapContacts = new GMaps({
    div: ".contact-map",
    lat: 55.835906,
    lng: 37.282262,
    zoom: 15,
    styles: mapStyle,
});

// mapContacts.addMarker({
//     lat: 55.879114,
//     lng: 37.544560,
//     icon: {
//         url: '/img/contacts/marker-object.png'
//     },
//     mouseover: function () {
//         $('.overlay').css('opacity', '1')
//     },
//     mouseout: function () {
//         $('.overlay').css('opacity', '0')
//     }
// });

// var markersObject = [{
//     "coords": {
//         "lat": 55.835906,
//         "lng": 37.282262
//     },
//     "type": 1,
//     "title": "Обект",
//     "ico": "/img/contacts/marker-object.png",
//     "click": ""
// }]
//
// markersObject.forEach(function(el) {
//     mapContacts.addMarker({
//         position: el.coords,
//         visible: true,
//         icon: {
//             url: el.ico
//         },
//         details: {
//             type: el.type
//         },
//         title: el.title,
//         click: function() {
//             el.click
//         }
//     });
// });

$(".office-object-links__item_office1").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");
    let icon = $link.data("icon");

    mapContacts.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: icon,
        details: {
            type: 1,
        },
        click: function() {
            $link.trigger("click");
        },
    });
});

$(".office-object-links__item_office2").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");
    let icon = $link.data("icon");

    mapContacts.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        visible: false,
        icon: icon,
        details: {
            type: 4,
        },
        click: function() {
            $link.trigger("click");
        },
    });
});

function getMapLink (type, coords) {
    let stringLink = '';
    if (type === 'yandex') {
        stringLink = `http://maps.yandex.ru/?text=${coords.lat},${coords.lng}`;
    }
    else if (type === 'google') {
        stringLink = `https://maps.google.com/?daddr=${coords.lat},${coords.lng}`;
    }
    else if (type === 'yandex-nav') {
        stringLink = `yandexnavi://build_route_on_map?lat_to=${coords.lat}&lon_to=${coords.lng}`;
    }

    return stringLink;
}

$('.contact-offices-item__accordionLink').on('click', function (e) {
    let $link = $(e.currentTarget);
    let $mapLinks = $('[data-tomap]');
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    $mapLinks.each((i, el) => {
        $(el).attr('href', `${getMapLink($(el).data('tomap'), {lat: lat, lng: lng} )}`)
    })
});

$('.contact-offices-types__link').on('click', function () {
    let $link = $('.contact-offices-item__accordionLink').eq(0);
    let $mapLinks = $('[data-tomap]');
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    $mapLinks.each((i, el) => {
        $(el).attr('href', `${getMapLink($(el).data('tomap'), {lat: lat, lng: lng} )}`)
    })
});

$(".office-object-links__item_office3").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");
    let icon = $link.data("icon");

    mapContacts.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: icon,
        visible: false,
        details: {
            type: 5,
        },
        click: function() {
            $link.trigger("click");
        },
    });
});

$(".office-center-links__item").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");
    let icon = $link.data("icon");

    mapContacts.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: icon,
        details: {
            type: 2,
        },
        click: function() {
            $link.trigger("click");
        },
    });
});

$(".region-list__link").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    mapContacts.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/contacts/marker-purple.png",
        details: {
            type: 3,
        },
        click: function() {
            $link.trigger("click");
        },
    });
});

var insertedMarkers = mapContacts.markers;

$("#office-object-link").click(function() {
    mapContacts.setCenter({
        lat: 55.835906,
        lng: 37.282262,
    });
    //mapContacts.setZoom(15);
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 1) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.setZoom(15);
    $(".contacts-header-links").removeClass("contacts-header-links_disable");
    $("#office-object").accordion({
        header: ".contact-offices-item__accordionLink",
        icons: false,
        heightStyle: 200,
        active: 0,
    });
});

$(".contact-offices-item__accordionLink_blue").click(function() {
    mapContacts.setCenter({
        lat: 55.835906,
        lng: 37.282262,
    });
    //mapContacts.setZoom(15);
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 4) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.setZoom(15);
    $(".contacts-header-links").removeClass("contacts-header-links_disable");
});

$(".contact-offices-item__accordionLink_orange").click(function() {
    mapContacts.setCenter({
        lat: 55.835906,
        lng: 37.282262,
    });
    //mapContacts.setZoom(15);
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 5) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.setZoom(15);
    $(".contacts-header-links").removeClass("contacts-header-links_disable");
});

$(".contact-offices-item__accordionLink_green").click(function() {
    mapContacts.setCenter({
        lat: 55.835906,
        lng: 37.282262,
    });
    //mapContacts.setZoom(15);
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 1) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.setZoom(15);
    $(".contacts-header-links").removeClass("contacts-header-links_disable");
});

$("#office-center-link").click(function() {
    mapContacts.setCenter({
        lat: 55.770851,
        lng: 37.622462,
    });
    //mapContacts.setZoom(18);
    //$('.contact-offices-item__type').hide();
    $("#center-office-text").show();

    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 2) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.fitZoom();
    $(".contacts-header-links").addClass("contacts-header-links_disable");
});

$("#office-region-link").click(function() {
    mapContacts.setCenter({
        lat: 55.879114,
        lng: 37.54456,
    });
    //mapContacts.setZoom(15);
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 3) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    mapContacts.fitZoom();
});

$(".region-list__link").click(function(e) {
    e.preventDefault();
    $(".region-item").hide();
    let lat = $(this).data("lat");
    let lng = $(this).data("lng");
    mapContacts.setZoom(15);
    mapContacts.setCenter({
        lat: lat,
        lng: lng,
    });

    let city = $(this).data("city-link");
    $(".region-list").hide();
    $("#" + city).show();

    $(".close-region-office").show();
    $(".contacts-header-links").addClass("contacts-header-links_disable");
});

$(".close-region-office").click(function() {
    $(".region-list").show();
    $(".region-item").hide();
    $(this).hide();
    mapContacts.fitZoom();
});

$(".office-object-links__item").click(function() {
    $(".contact-offices-item__type").hide();
    let office = $(this).data("office");
    $("#" + office).show();
});

$(".office-center-links__item").click(function() {
    $(".contact-offices-item__type").hide();
    let office = $(this).data("office");
    $("#" + office).show();
});

$(".contact-offices-item__text").mCustomScrollbar({
    scrollInertia: 0,
    mouseWheel: { preventDefault: true },
});

$("#office-object").accordion({
    header: ".contact-offices-item__accordionLink",
    icons: false,
    heightStyle: 200,
    active: 0,
});

function teamScroll() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $("[data-team]").mCustomScrollbar('destroy');
    } else {
        $("[data-team]").mCustomScrollbar({
            scrollInertia: 0,
            mouseWheel: { preventDefault: true },
        });
    }
}
function teamScrollXS() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $('.contact-offices-types-tabs__nav').css('width', $(window).width() + 'px');
    } else {
        $('.contact-offices-types-tabs__nav').css('width', '');
    }
}
teamScroll();
setTimeout(teamScrollXS, 200);
$(window).on('resize', function () {
    teamScroll();
    teamScrollXS();
});