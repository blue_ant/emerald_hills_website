$(".wrapper")
    .addClass("wrapper-forLongread")
    .find(".longread-banner")
    .appendTo("body");

{
    let $win = $(window);
    let $header = $(".longread-banner");
    $win.on("scroll", (event) => {
        event.preventDefault();
        let v = 330 - $win.scrollTop();
        if (v) {
            window.requestAnimationFrame(() => {
                $header.css("height", `${v}px`);
            });
        }
    });
}

$(function() {
    (function($gallery) {
        if (!$gallery.length) return;

        var gallery = new Swiper($gallery, {
            slidesPerView: 'auto',
            loop: true,
            centeredSlides: true,
            breakpoints: {
                1023: {
                    slidesPerView: 1
                }
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            speed: 600,
            initialSlide: 2
        });
    })($('[data-longread-slider]'));
});