let photosResidentsOffset = 0;

let getPhotos = () => {
    $.ajax({
        url: $('.photos-residents').data('photos-residents'),
        dataType: "json",
        method: 'GET',
        data: {
            action: 'get_post',
            limit: 8,
            offset: photosResidentsOffset
        }

    })
        .done((jsonResponse) => {
            let photos = jsonResponse.post;
            let colOne = $('.photos-residents__col:nth-child(1)');
            let colTwo = $('.photos-residents__col:nth-child(2)');
            console.log('photos', photos);
            console.log('photos length', photos.length);
            let morePhotosBtn = $('.photos-residents__btn');

            if (photos.length) {
                _.forEach(photos, function(post, index) {
                    if (index % 2 === 0) {
                        colOne.append('<div class="photos-residents__item">'+ post.html +'</div>');
                    } else {
                        colTwo.append('<div class="photos-residents__item">'+ post.html +'</div>');
                    }
                });
                morePhotosBtn.removeClass('photos-residents__btn_hidden');
            } else {
                morePhotosBtn.addClass('photos-residents__btn_hidden');
            }

            if (photos.length < 8) {
                morePhotosBtn.addClass('photos-residents__btn_hidden');
            } else {
                morePhotosBtn.removeClass('photos-residents__btn_hidden');
            }
            if ( typeof window.instgrm !== 'undefined' ) {
                window.instgrm.Embeds.process();
            }


        })
        .fail(() => {
            alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
        });
}

getPhotos();

$('.photos-residents__btn').on('click', (event) => {
    event.preventDefault();
    photosResidentsOffset = photosResidentsOffset + 8;
    console.log('photosResidentsOffset', photosResidentsOffset);
    getPhotos();
})