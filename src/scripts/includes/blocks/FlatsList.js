class FlatsList {
    constructor(opts) {
        this.tpl = _.template(opts.tpl.replace(/\s{2,}/g, ''));
        this.additionalClassName = opts.additionalClassName;
        this.opts = opts;
    }

    render(cardsArray) {
        let tpl = this.tpl;
        let listMarkup = tpl(cardsArray);
        this.opts.$mountEl.html(listMarkup); 
    }
}