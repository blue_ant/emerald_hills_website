class ParkingFilter {
    constructor(
        el,
        opts = {
            submitHandler: $.noop,
        }
    ) {
        if (!el) {
            console.error('Filter class constructor requires "el" argument!');
            return;
        }

        this.offset = 0;

        const submitHandler = _.debounce(opts.submitHandler, 600);

        let $filterForm = $(el);
        this.$filterForm = $filterForm;

        this.restoreStateFromURL();

        if (!$filterForm.length) {
            console.error('FilterForm constructor can\'t find given "el" in DOM!');
            return;
        }


        let $inputs = $($filterForm.get(0).elements);

        $inputs.on("change", (event) => {
            event.preventDefault();
            this.offset = 0;
            $filterForm.trigger("submit");
        });

        $filterForm.on("submit", (event) => {
            event.preventDefault();
            submitHandler($filterForm);
            this.saveStateToURL();
            return false;
        });


        // remember thirdparties url params
        {
            let urlParams = parseQueryString(window.location.search);

            $inputs.each((index, el)=> {
                if (urlParams[el.name]) {
                    delete urlParams[el.name];
                }
            });

            let otherPossibleParams = [
                "flat_num",
                "sort",
                "sortdir",
                "offset",
            ];

            for (var i = otherPossibleParams.length - 1; i >= 0; i--) {
                delete urlParams[otherPossibleParams[i]];
            }

            let thirdpartyUrlParamsStr = "";
            for (let key in urlParams) {
                thirdpartyUrlParamsStr += `&${key}=${urlParams[key]}`;
            }

            this.thirdpartyUrlParamsStr = thirdpartyUrlParamsStr;
        }

        this.$inputs = $inputs;

        $filterForm.trigger("submit");
    }

    saveStateToURL() {
        let queryStr = this.$filterForm.serialize() + `&offset=${this.offset}`;
        let newURL = location.pathname + "?" + queryStr + this.thirdpartyUrlParamsStr;
        history.replaceState({}, document.title, newURL);
    }

    restoreStateFromURL() {
        let storedValues = parseQueryString(window.location.search);

        // console.log("restored form states:", storedValues);
        let $inps = $(this.$filterForm.get(0).elements);


    }
}
