function stayHomeItemHeight() {
    let $stayHomeItem = $('[data-stayhome-item]');
    let $stayHomeItemCount = $stayHomeItem.length / 2 + 1;

    for (let i = 2; i <= $stayHomeItemCount; i++) {
        let dataA = '[data-stayhome-item-' + i + 'a]';
        let dataB = '[data-stayhome-item-' + i + 'b]';

        let $stayHomeItemA = $(dataA);
        let $stayHomeItemHeightA = $stayHomeItemA.find('.StayHomeSteps_txt').height() + 42;

        let $stayHomeItemB = $(dataB);
        let $stayHomeItemHeightB = $stayHomeItemB.find('.StayHomeSteps_txt').height() + 42;

        if (window.matchMedia("(min-width: 1024px)").matches) {
            if ($stayHomeItemHeightA > $stayHomeItemHeightB) {
                $stayHomeItemA.css('height', $stayHomeItemHeightA + 'px');
                $stayHomeItemB.css('height', $stayHomeItemHeightA + 'px');
            } else {
                $stayHomeItemA.css('height', $stayHomeItemHeightB + 'px');
                $stayHomeItemB.css('height', $stayHomeItemHeightB + 'px');
            }
        } else {
            $stayHomeItemA.css('height', '');
            $stayHomeItemB.css('height', '');
        }
    }
}

stayHomeItemHeight();
$(window).on('resize', function () {
    stayHomeItemHeight();
});