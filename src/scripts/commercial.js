$(function() {
    let actNum = window.location.hash;

    if ( actNum === "#Sale" || actNum === "") {
        actNum = 0;
    } else {
        actNum = 1;
    }

    let decorTabs = $("#CommercialTabs");

    let url = window.location.href;

    let urlLenght = url.length;
    if ( !!(~url.indexOf('#')) == true ) {
        for(let i=0; i <= urlLenght; i++) {
            if ( url[i] === '#' ) {
                url = url.slice(0, urlLenght - (urlLenght - i) )
            }
        }
    }

    let initTabs = (actNum) => {
        decorTabs.tabs({
            active: actNum,
            activate: function (event, ui) {
							  let scrollTop = $(window).scrollTop();
                //window.location.hash = ui.newPanel.attr('data-hash');
                let hash = ui.newPanel.attr('data-hash');
                let newUrl = `${url}#${hash}`;
                window.history.replaceState(undefined, undefined, newUrl);
                $(window).scrollTop(scrollTop);
            }
        });
    };
    initTabs(actNum);
		/*$( "#CommercialTabs" ).tabs({
			active: 0
		});*/
});
